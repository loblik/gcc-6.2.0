/* Exported function prototypes from the TI TINY backend.
   Copyright (C) 2012-2015 Free Software Foundation, Inc.
   Contributed by Red Hat.

   This file is part of GCC.

   GCC is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GCC is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GCC; see the file COPYING3.  If not see
   <http://www.gnu.org/licenses/>.  */

#ifndef GCC_TINY_PROTOS_H
#define GCC_TINY_PROTOS_H

rtx	tiny_eh_return_stackadj_rtx (void);
void	tiny_expand_eh_return (rtx);
void	tiny_expand_epilogue (int);
void	tiny_expand_helper (rtx *operands, const char *, bool);
void	tiny_expand_prologue (void);
const char * tinyx_extendhisi (rtx *);
void	tiny_fixup_compare_operands (machine_mode, rtx *);
int	tiny_hard_regno_mode_ok (int, machine_mode);
int	tiny_hard_regno_nregs (int, machine_mode);
int	tiny_hard_regno_nregs_has_padding (int, machine_mode);
int	tiny_hard_regno_nregs_with_padding (int, machine_mode);
bool    tiny_hwmult_enabled (void);
rtx	tiny_incoming_return_addr_rtx (void);
int tiny_starting_frame_offset();
void	tiny_init_cumulative_args (CUMULATIVE_ARGS *, tree, rtx, tree, int);
int	tiny_initial_elimination_offset (int, int);
bool    tiny_is_interrupt_func (void);
const char * tinyx_logical_shift_right (rtx);
const char * tiny_mcu_name (void);
bool	tiny_modes_tieable_p (machine_mode, machine_mode);
void	tiny_output_labelref (FILE *, const char *);
void	tiny_register_pragmas (void);
rtx	tiny_return_addr_rtx (int);
void	tiny_split_movsi (rtx *);
void    tiny_start_function (FILE *, const char *, tree);
rtx	tiny_subreg (machine_mode, rtx, machine_mode, int);
bool    tiny_use_f5_series_hwmult (void);

#endif /* GCC_TINY_PROTOS_H */
